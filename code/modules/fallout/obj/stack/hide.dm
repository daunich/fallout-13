//Fallout 13 animal hides directory

/obj/item/stack/sheet/animalhide/gecko
	name = "gecko skin"
	desc = "A perfect clothing and armor crafting material."
	singular_name = "gecko skin piece"
	icon = 'icons/fallout/objects/items.dmi'
	icon_state = "sheet-geckohide"
	origin_tech = null

/obj/item/stack/sheet/animalhide/molerat
	name = "molerat skin"
	desc = "A smelly piece of a hide, mostly used as a doormat."
	singular_name = "molerat skin piece"
	icon = 'icons/fallout/objects/items.dmi'
	icon_state = "sheet-molerat"
	origin_tech = null

/obj/item/stack/sheet/animalhide/deathclaw
	name = "deathclaw skin"
	desc = "A glorious hunting trophy."
	singular_name = "deathclaw skin piece"
	icon = 'icons/fallout/objects/items.dmi'
	icon_state = "sheet-deathclaw"
	origin_tech = null

/obj/item/stack/sheet/animalhide/wolf
	name = "dog skin"
	desc = "Once upon a time, lived a dog.<br>It was not a usual dog, because it lived out in the wild wasteland, where little kids are not allowed to play.<br>One day, a said dog got mad and got into a fight with an adventurer.<br>An adventurer was stronger, and the dog was no more.<br>But a little part of this dog, lives on to this day."
	singular_name = "dog skin piece"
	icon = 'icons/fallout/objects/items.dmi'
	icon_state = "sheet-dog"
	origin_tech = null