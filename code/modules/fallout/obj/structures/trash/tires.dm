//Fallout 13 automobile tires directory

/obj/structure/tires
	name = "tire"
	desc = "A ring-shaped vehicle component that covers the wheel's rim to protect it and enable better vehicle performance."
	obj_integrity = 50
	max_integrity = 50
	icon_state = "one_t"
	density = 1
	self_weight = 5
	icon = 'icons/fallout/objects/structures/trash.dmi'

/obj/structure/tires/half
	name = "half tire"
	desc = "A part of a part of a car." //The description is totally intentional.
	obj_integrity = 10
	max_integrity = 10
	icon_state = "half_t"
	density = 1
	icon = 'icons/fallout/objects/structures/trash.dmi'
	anchored = 1

/obj/structure/tires/two
	name = "couple of tires"
	desc = "A ring-shaped vehicle components that cover the wheel's rim to protect it and enable better vehicle performance."
	obj_integrity = 50
	max_integrity = 50
	icon_state = "two_t"
	density = 1
	icon = 'icons/fallout/objects/structures/trash.dmi'
	anchored = 1

/obj/structure/tires/five
	name = "pile of tires"
	desc = "Five tires stacked over each other in an orderly fashion.<br>Is the car shop nearby or something?"
	obj_integrity = 50
	max_integrity = 50
	icon_state = "five_t"
	density = 1
	icon = 'icons/fallout/objects/structures/trash.dmi'
	anchored = 1
