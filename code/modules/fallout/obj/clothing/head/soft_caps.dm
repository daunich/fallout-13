//Fallout 13 soft caps directory

/obj/item/clothing/head/soft/f13
	icon = 'icons/fallout/clothing/hats.dmi'
	self_weight = 0.2

/obj/item/clothing/head/soft/f13/baseball
	name = "baseball cap"
	desc = "A type of soft cap with a rounded crown and a stiff peak projecting in front."
	icon_state = "baseball"
	item_color = "baseball"
	flags_inv = HIDEHAIR

/obj/item/clothing/head/soft/f13/enclave
	name = "officer hat"
	desc = "A standard-issue Enclave officer's cap."
	icon_state = "enclave"
	item_color = "enclave"
	flags_inv = HIDEHAIR